package sda.fight;

public class App {
    public static void main(String[] args) {
        Menu menu = new Menu();


        menu.setImieZawodnika(menu.whatsYourName());
        IFighter boxer1 = menu.produceFighter(menu.factoryFighter());
        menu.setImieZawodnika(menu.whatsYourName());
        IFighter boxer2 = menu.produceFighter(menu.factoryFighter());


        IFightingMatch match = new BoxingMatch(boxer1, boxer2);
        match.fight();
    }
}