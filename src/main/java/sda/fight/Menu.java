package sda.fight;

import java.util.Scanner;

public class Menu {
    Scanner sc = new Scanner(System.in);
    String imieZawodnika;

    public int factoryFighter() {
        System.out.println("1.Boxer \n2.AggressiveBoxer");
        int nrFighter = sc.nextInt();
        return nrFighter;
    }

    public IFighter produceFighter(int nrFighter) {
        if (nrFighter == 1) {

            IFighter boxer1 = new AggressiveBoxer(imieZawodnika, 50, IFighterStyle.BALANCED, 10);
            return boxer1;
        }
        if (nrFighter == 2) {

            IFighter boxer2 = new Boxer(imieZawodnika, 50, IFighterStyle.BALANCED, 7);
            return boxer2;
        }
        return null;
    }

    public String whatsYourName() {
        System.out.println("Jakie imie ma zawodnik?");
        String imie = sc.next();
        return imie;
    }

    public void setImieZawodnika(String imieZawodnika) {
        this.imieZawodnika = imieZawodnika;
    }
}
